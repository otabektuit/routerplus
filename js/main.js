let closeNavbar = $('#navbarClose');
let openNavber = $('#navbarOpen');
let navbarNav = $('#navbarNav');
let mdNavbar = $('#md-navbar');
let mainNav = $('#mainNav');
let links = $('.nav-to');

closeNavbar.on('click',function(){
	mdNavbar.css({'width':'0%','z-index':'-1'});
})

openNavber.on('click',function(){
	mdNavbar.css({'width':'80%','z-index':'999999'});
})

$(window).on('scroll',function(){

	if ($(this).scrollTop()>50) {
		mainNav.removeClass('bg-transparent');
		mainNav.addClass('bg-primary');
	}
	if ($(this).scrollTop()===0) {
		mainNav.addClass('bg-transparent');
		mainNav.removeClass('bg-primary');	}
});

links.click(function () {
	let to = $(this).attr('data-to');
	let height = $('#'+to).offset().top;

	$('html, body').animate({
		scrollTop: $("#"+to).offset().top
	}, 1000);
	console.log(to,height )
})

